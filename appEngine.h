#pragma once

#include <QObject>
#include <QVector>


struct bookmark {
    QString name;
    double beginTime;
    double milliseconds;
};

class appEngine : public QObject
{
    Q_OBJECT

public:
    appEngine(QObject* parent = nullptr);
    ~appEngine();

    Q_INVOKABLE  QString name(const int &index) const;
    Q_INVOKABLE  QString g_name(const int &index) const;
    Q_INVOKABLE  QString s_name(const int &index) const;
    Q_INVOKABLE  double beginTime(const int &index) const;
    Q_INVOKABLE  double g_beginTime(const int &index) const;
    Q_INVOKABLE  double s_beginTime(const int &index) const;
    Q_INVOKABLE  double milliseconds(const int &index) const;
    Q_INVOKABLE  double g_milliseconds(const int &index) const;
    Q_INVOKABLE  double s_milliseconds(const int &index) const;


    Q_INVOKABLE  int sizeList() const;
    Q_INVOKABLE  int g_sizeList() const;
    Q_INVOKABLE  int s_sizeList() const;

private:
    QVector<bookmark> listBookmarks;
    QVector<bookmark> groupedBookmarks;
    QVector<bookmark> singleBookmarks;

    bookmark newbookmark;
signals:
    void listBookmarksChanged();

public slots:
    void setListBookmarks(const int &quantity);
    void groupBookmarks(const double &minutesInPixel);
};
