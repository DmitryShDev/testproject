#include <QDebug>
#include <QMessageBox>
#include <algorithm>
#include "random.h"
#include "appEngine.h"

appEngine::appEngine(QObject *parent)
{
    Q_UNUSED(parent);

    listBookmarks = {};
    newbookmark = {};
    groupedBookmarks = {};
    singleBookmarks = {};
}

appEngine::~appEngine()
{

}

void appEngine::setListBookmarks(const int& quantity)
{
    listBookmarks.clear();

    for(int i = 0; i < quantity; i++)
    {
        listBookmarks.append(newbookmark);
        listBookmarks[i].name = QString("Bookmark %1").arg(i+1);
        listBookmarks[i].beginTime = Random::get(1, 1440);
        listBookmarks[i].milliseconds = Random::get(100000, 648000);
    }


    std::sort(listBookmarks.begin(), listBookmarks.end(), [](bookmark const& a, bookmark const& b)
    {
        return a.beginTime < b.beginTime;
    });

}

void appEngine::groupBookmarks(const double& minutesInPixel)
{
    groupedBookmarks.clear();
    singleBookmarks.clear();
    bookmark first1 = listBookmarks[0];
    int countGroupedBookmarks = 0;
    double compareTime = 0;
    for (int i = 1; i < listBookmarks.size(); i++)
    {
        if((listBookmarks[i].beginTime - first1.beginTime) > minutesInPixel * 100 && countGroupedBookmarks > 0)
        {
            groupedBookmarks.append(newbookmark);
            groupedBookmarks.last().name = QString::number(countGroupedBookmarks + 1);
            groupedBookmarks.last().beginTime = first1.beginTime;
            groupedBookmarks.last().milliseconds = compareTime;
            first1 = listBookmarks[i];
            countGroupedBookmarks = 0;
        }
        else if((listBookmarks[i].beginTime - first1.beginTime) > minutesInPixel * 100) {
            singleBookmarks.append(first1);
            first1 = listBookmarks[i];
        }
        else {
            countGroupedBookmarks++;
            if (first1.milliseconds < listBookmarks[i].milliseconds) {
                double z = (listBookmarks[i].beginTime - first1.beginTime) * 60 * 60;
                compareTime = z + listBookmarks[i].milliseconds;
            }
            else
                compareTime = first1.milliseconds;

            if(listBookmarks.size() - 1 == i) {
                groupedBookmarks.append(newbookmark);
                groupedBookmarks.last().name = QString::number(countGroupedBookmarks + 1);
                groupedBookmarks.last().beginTime = first1.beginTime;
                groupedBookmarks.last().milliseconds = compareTime;
            }
        }
    }
}



QString appEngine::name(const int& index) const
{
    return listBookmarks[index].name;
}

QString appEngine::g_name(const int& index) const
{
    return groupedBookmarks[index].name;
}

QString appEngine::s_name(const int &index) const
{
    return singleBookmarks[index].name;
}

double appEngine::beginTime(const int& index) const
{
    return listBookmarks[index].beginTime;
}

double appEngine::g_beginTime(const int& index) const
{
    return groupedBookmarks[index].beginTime;
}

double appEngine::s_beginTime(const int &index) const
{
    return singleBookmarks[index].beginTime;
}

double appEngine::milliseconds(const int& index) const
{
    return listBookmarks[index].milliseconds;
}

double appEngine::g_milliseconds(const int& index) const
{
    return groupedBookmarks[index].milliseconds;
}

double appEngine::s_milliseconds(const int &index) const
{
    return singleBookmarks[index].milliseconds;
}

int appEngine::sizeList() const
{
    return listBookmarks.size();
}

int appEngine::g_sizeList() const
{
    return groupedBookmarks.size();
}

int appEngine::s_sizeList() const
{
    return singleBookmarks.size();
}
