import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2

// color #1465d9
// color #69cd77

Rectangle {
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 10
    color: "#AA5d93e5"
    border.color: "blue"


    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onEntered: {
            label.text = name2.text
        }
        onExited: label.text = ""
    }


    
    Text {
        id: name2
        text: qsTr(bookmarksObj.s_name(index))
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        color: "white"
        font {
            pointSize: 20
        }
    }
}
