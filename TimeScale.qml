import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12


Rectangle {
    id:mainFrame
    border.color: "black"
    anchors.centerIn: parent
    
    ListView {
        id: listview
        height: mainFrame.height
        width: mainFrame.width
        anchors.top: parent.top
        anchors.left: parent.left
        model: 24
        
        orientation: ListView.Horizontal

        delegate: Rectangle {
            id:separator
            width: mainFrame.width/24
            height: mainFrame.height/4
            border.color: "black"
            Rectangle {
                id:bottomline
                height: 2
                width: separator.width
                anchors.bottom: separator.bottom
                border.color: "white"
            }
            
            Text {
                id: hour
                anchors.left: separator.left
                anchors.leftMargin: -5
                anchors.bottom: separator.bottom
                anchors.bottomMargin: -15
                
                text: qsTr(index + "h")
                font {
                    pointSize: 10
                }
            }
            
        }

    }
}
