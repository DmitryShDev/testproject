import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2

// color #1465d9
// color #69cd77

ApplicationWindow {
    id: appWindow
    y: Screen.desktopAvailableHeight - Screen.height/1.5
    visible: true
    width: Screen.width
    height: 480
    title: qsTr("Test Task")


    onWidthChanged: {
        time.start();
    }

    Timer {
        id: time
        interval: 1000;running: false; repeat: false
        onTriggered: {
                    if (repeater.model >= 0) {
                        bookmarksObj.groupBookmarks(minutesInPixel)
                        repeater.model = bookmarksObj.s_sizeList()
                        repeater2.model = bookmarksObj.g_sizeList()
                    }
        }
    }

    property double pixelsInHour: mainFrame.width / 24
    property double pixelsInMilliseconds: mainFrame.width / 24 / 60 / 60 / 60
    property double pixelsInMinute: mainFrame.width / 24 / 60
    property double minutesInPixel: 1440 / mainFrame.width


    CreateBookmarksDialog {
        id: createBookmarksDialog
    }

    function createBookmarks() {
        createBookmarksDialog.open()
    }

    Button {
        id:createBookmarksButton
        width: 200
        height: 50
        text: "Create bookmarks"
        onClicked: {
            createBookmarks()
        }

    }

//    Button {
//        id: groupBookmarks
//        width: 200
//        height: 50
//        text: "Group bookmarks"
//        anchors.left: parent.left
//        anchors.leftMargin: 300
//        onClicked: {
//            bookmarksObj.groupBookmarks(minutesInPixel)
//            repeater.model = bookmarksObj.s_sizeList()
//            repeater2.model = bookmarksObj.g_sizeList()
//        }
//    }

    TimeScale {
        id: mainFrame
        width: parent.width
        height: parent.height / 4

            Repeater {
            id:repeater
            delegate: Bookmark {
                id: bookmark
                height: mainFrame.height / 2
                width: bookmarksObj.s_milliseconds(index) * pixelsInMilliseconds
                x: bookmarksObj.s_beginTime(index) * pixelsInMinute
            }
         }
            Repeater {
                id:repeater2

                delegate: Rectangle {
                        id:groupedBookmark
                        width: bookmarksObj.g_milliseconds(index) * pixelsInMilliseconds
                        x: bookmarksObj.g_beginTime(index) * pixelsInMinute
                        height: mainFrame.height / 2
                        anchors.bottom: mainFrame.bottom
                        anchors.bottomMargin: 10
                        color: "#AA69cd77"
                        border.color: "green"

                        MouseArea {
                            anchors.fill: parent
                            hoverEnabled: true
                            onEntered: label.text = txtGroupedBookmark.text
                            onExited: label.text = ""
                        }

                        Text {
                            id: txtGroupedBookmark
                            text: qsTr(bookmarksObj.g_name(index))
                            anchors.left: parent.left
                            anchors.leftMargin: 10
                            anchors.verticalCenter: parent.verticalCenter
                            color: "white"
                            font {
                                pointSize: 20
                            }
                        }
                    }
           }
    }

    Label {
        id: label
        anchors.top: mainFrame.bottom
        anchors.topMargin: 20
        anchors.left: mainFrame.left
        anchors.leftMargin: 50
    }

}
