import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2

// color #1465d9
// color #69cd77

Dialog {
    id:createBookmarksDialog
    width: 300
    height: 150
    title: qsTr("Create bookmars")
    standardButtons: StandardButton.Apply | StandardButton.Cancel


    onApply: {
        if (quantitybookmarksTF.text > 0)
        {
            bookmarksObj.setListBookmarks(quantitybookmarksTF.text)
            bookmarksObj.groupBookmarks(minutesInPixel)
            repeater.model = 0
            repeater.model = bookmarksObj.s_sizeList()
            repeater2.model = 0
            repeater2.model = bookmarksObj.g_sizeList()
            createBookmarksDialog.close()
        }
    }


    TextField {
        id: quantitybookmarksTF
        anchors.centerIn: parent
        focus: true
        placeholderText: qsTr("Enter quantity of bookmarks")
        validator: IntValidator {bottom: 1; top: 100000000}
    }

    Text {
        id: textdialog
        text: qsTr("How many bookmarks do you want to create ?")
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 25
    }
}
